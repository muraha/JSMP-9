let
  gulp = require('gulp'),
  scss = require('gulp-sass'),
  autoprefixer = require('gulp-autoprefixer'),
  lint = require('gulp-stylelint'),
  sourcemaps = require('gulp-sourcemaps'),
  browserSync = require('browser-sync')

/*scss*/
gulp.task('scss', () =>
  gulp
  .src('app/scss/**/*.scss')
  .pipe(sourcemaps.init())
  .pipe(scss().on('error', scss.logError))
  .pipe(autoprefixer(['last 15 versions', '> 1%', 'ie 8', 'ie 7'], {
    cascade: true
  }))
  .pipe(sourcemaps.write('.'))
  .pipe(gulp.dest('app/css'))
  .pipe(browserSync.reload({
    stream: true
  }))
);

/*linter*/
gulp.task('lint', () =>
  gulp.src('app/scss/**/*.scss')
  .pipe(lint({
    failAfterError: true,
    reporters: [{
      formatter: 'verbose',
      console: true
    }],
    debug: true
  }))
);

gulp.task('liveserver', () =>
  browserSync({
    server: {
      baseDir: 'app'
    },
    notify: false
  })
);



gulp.task('watch', ['liveserver'], function() {
  gulp.watch('app/scss/**/*.scss', ['scss']);
  gulp.watch('app/*.html', browserSync.reload);
});



gulp.task('default', ['watch']);

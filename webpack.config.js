const webpack = require('webpack');
const path = require('path');
const src = path.resolve(__dirname, 'app');
const dist = path.resolve(__dirname, 'dist');
const coffee = require("coffeescript");

// const images = path.resolve(src, 'images');
// const config = path.resolve(src, 'config');

module.exports = {
  entry: "./app/index.js", // string | object | array
  // Here the application starts executing
  // and webpack starts bundling

  output: {
    path: dist,
    filename: "[name].js",
    // publicPath: "/",
    // library: "MyLibrary",
    // libraryTarget: "umd",
  },

  module: {
    // configuration regarding modules

    rules: [
      // rules for modules (configure loaders, parser options, etc.)
      {
        test: /\.(.sx?$|coffee$)/,
        exclude: /(node_modules|bower_components|plato)/,
        use: [{
            loader: 'babel-loader',
            options: {
              presets: ['@babel/preset-env']
            }
          },
          {
            loader: "ts-loader"
          },
          {
            loader: 'coffee-loader',
            options: {
              literate: true
            }
          }
        ]
      },
      {
        test: /\.p?css$/,
        // include: [src, 'webpack/dev-server-local-test-tools'],
        use: [{
          loader: 'style-loader'
        }, {
          loader: 'css-loader',
          options: {
            modules: true,
            sourceMap: false,
            localIdentName: '[name]__[local]--[hash:base64:5]' //must be the same as for react-css-modules
          }
        }, {
          loader: 'postcss-loader'
        }]
      },


      {
        test: /\.html$/,

        use: [
          // apply multiple loaders and options
          {
            loader: "html-loader",
            options: {
              /* ... */
            }
          }
        ]
      },

      {
        oneOf: [ /* rules */ ]
      },
      // only use one of these nested rules

      {
        rules: [ /* rules */ ]
      },
      // use all of these nested rules (combine with conditions to be useful)

      {
        resource: {
          and: [ /* conditions */ ]
        }
      },
      // matches only if all conditions are matched

      {
        resource: {
          or: [ /* conditions */ ]
        }
      },
      {
        resource: [ /* conditions */ ]
      },
      // matches if any condition is matched (default for arrays)


    ],

    /* Advanced module configuration (click to show) */
  },

  resolve: {
    // options for resolving module requests
    // (does not apply to resolving to loaders)

    modules: [
      "node_modules",
      path.resolve(__dirname, "app")
    ],
    // directories where to look for modules

    extensions: [".js", ".json", ".jsx", ".css"],
    // extensions that are used

    alias: {
      // a list of module name aliases

      "module": "new-module",
      // alias "module" -> "new-module" and "module/path/file" -> "new-module/path/file"

      "only-module$": "new-module",
      // alias "only-module" -> "new-module", but not "only-module/path/file" -> "new-module/path/file"

      "module": path.resolve(__dirname, "app/third/module.js"),
      // alias "module" -> "./app/third/module.js" and "module/file" results in error
      // modules aliases are imported relative to the current context
    },
    /* alternative alias syntax (click to show) */

    /* Advanced resolve configuration (click to show) */
  },

  performance: {
    hints: "warning", // enum
    maxAssetSize: 200000, // int (in bytes),
    maxEntrypointSize: 400000, // int (in bytes)
    assetFilter: function(assetFilename) {
      // Function predicate that provides asset filenames
      return assetFilename.endsWith('.css') || assetFilename.endsWith('.js');
    }
  },

  devtool: "source-map", // enum
  // enhance debugging by adding meta info for the browser devtools
  // source-map most detailed at the expense of build speed.

  context: __dirname, // string (absolute path!)
  // the home directory for webpack
  // the entry and module.rules.loader option
  //   is resolved relative to this directory

  target: "web", // enum
  // the environment in which the bundle should run
  // changes chunk loading behavior and available modules

  externals: ["react", /^@angular\//],
  // Don't follow/bundle these modules, but request them at runtime from the environment

  stats: "errors-only",
  // lets you precisely control what bundle information gets displayed

  devServer: {
    proxy: { // proxy URLs to backend development server
      '/api': 'http://localhost:3000'
    },
    contentBase: path.join(__dirname, 'public'), // boolean | string | array, static file location
    compress: true, // enable gzip compression
    historyApiFallback: true, // true for index.html upon 404, object for multiple paths
    hot: true, // hot module replacement. Depends on HotModuleReplacementPlugin
    https: false, // true for self-signed, object for cert authority
    noInfo: true, // only errors & warns on hot reload
    // ...
  },

  plugins: [
    // ...
  ],
  // list of additional plugins


  /* Advanced configuration (click to show) */
}

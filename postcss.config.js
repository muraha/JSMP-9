module.exports = {
  plugins: [
    require('postcss-import'),
    require('postcss-global-import'),
    require('postcss-extend'),
    require('postcss-mixins'),
    require('postcss-nested'),
    require('autoprefixer'),
    require('postcss-css-variables'),
    require('postcss-calc'),
   // require('cssnano')
  ]
};
